
#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <windows.h>
#include <iostream>
#include <intrin.h>
#include <cstring>

#include "Methods.h"

using namespace std;
int main()
{
	
	isDebug = true; // true print Name, false - print pseudoname

	INT32 count_methods = 36;
	
	Cl_Methods Methods[]{
		Cl_Methods("Scsi port bus target id logical unit id 0 identifier", "SPBT", HARDWARE_DEVICEMAP_Scsi)
		,Cl_Methods("HKLM\\HARDWARE\\Description\\System \"SystemBiosVersion\"", "SBV", SystemBiosVersion)
		,Cl_Methods("Reg key (HKLM\\SOFTWARE\\Oracle\\VirtualBox Guest Additions)", "VGA", VirtualBox_Guest_Additions)
		,Cl_Methods("Reg key(HKLM\\HARDWARE\\Description\\System \"VideoBiosVersion\")", "VBV", VideoBiosVersion)
		,Cl_Methods("Reg key (HKLM\\HARDWARE\\ACPI\\DSDT\\VBOX)", "ADV", ACPI_DSDT_VBOX)
		,Cl_Methods("Reg key (HKLM\\HARDWARE\\ACPI\\FADT\\VBOX)", "AFV", ACPI_FADT_VBOX)
		,Cl_Methods("Reg key (HKLM\\HARDWARE\\ACPI\\RSDT\\VBOX)", "ARV", ACPI_RSDT_VBOX)
		,Cl_Methods("Reg key (HKLM\\SYSTEM\\ControlSet001\\Services\\VBox)", "CSV", ControlSet001_Services)
		,Cl_Methods("Reg key (HKLM\\HARDWARE\\DESCRIPTION\\System \"SystemBiosDate\"", "SBD", SystemBiosDate)
		,Cl_Methods("Driver files in C:\\WINDOWS\\system32\\drivers\\VBox", "DVB", drivers)
		,Cl_Methods("Additional system files", "ASF", system32_vbox)
		,Cl_Methods("Looking for a MAC address starting with 08:00:27", "MSW", check_mac_x08_x00_x27)
		,Cl_Methods("Looking for pseudo devices", "PD", pseudo_devices)
		,Cl_Methods("Looking for VBoxTray windows", "VTW", VBoxTray_windows)
		,Cl_Methods("Looking for VBox network share", "VNS", VirtualBox_Shared_Folders)
		,Cl_Methods("Looking for VBox processes (vboxservice.exe, vboxtray.exe)", "VBP", vboxservice_vboxtray)
		,Cl_Methods("Looking for VBox devices using WMI", "VDW", VBox_devices_WMI)
		,Cl_Methods("NICCONFIG", "NC", NICCONFIG)
		,Cl_Methods("sysdriver", "sd", sysdriver)
		,Cl_Methods("NTEventLog", "NEL", NTEventLog)
		,Cl_Methods("bios", "b", bios)
		,Cl_Methods("diskdrive", "dd", diskdrive)
		,Cl_Methods("Startup", "sup", Startup)
		,Cl_Methods("ComputerSystem", "CS", ComputerSystem)
		,Cl_Methods("service", "s", service)
		,Cl_Methods("LogicalDisk", "LD", LogicalDisk)
		,Cl_Methods("Win32_LocalProgramGroup", "WLP", Win32_LocalProgramGroup)
		,Cl_Methods("NIC", "N", NIC)
		,Cl_Methods("process", "p", process)
		,Cl_Methods("BaseBoard", "BB", BaseBoard)
		,Cl_Methods("SystemEnclosure", "SE", SystemEnclosure)
		,Cl_Methods("cdrom", "c", cdrom)
		,Cl_Methods("netclient", "nc", netclient)
		,Cl_Methods("csproduct", "csp", csproduct)
		,Cl_Methods("Win32_VideoController", "WVC", Win32_VideoController)
		,Cl_Methods("Win32_PnPEntity", "WPP", Win32_PnPEntity)
	};

	char OS_INFO_STR[32];

	//INFO OS
	OSVERSIONINFO OS_INFO;
	OS_INFO.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
	GetVersionEx(&OS_INFO);
	_snprintf(OS_INFO_STR, sizeof(OS_INFO_STR) - sizeof(OS_INFO_STR[0]),"%lu.%lu build %lu", OS_INFO.dwMajorVersion,	OS_INFO.dwMinorVersion, OS_INFO.dwBuildNumber);
	
	// CPU  
	string vendor, brand;
	vendor= get_vendor();
	brand = get_brand();

	printf("Windows version: %s\n", OS_INFO_STR);
	printf("CPU: %s %s \n", vendor.c_str(), brand.c_str());

	//Info VirtualBox 
	printf("%s\n", "VirtualBox Info");

	for (int i = 0;i < count_methods;i++)
	{
		if (isDebug)
			check(Methods[i].Name, Methods[i].functionPtr);
		else
			check(Methods[i].PseudoName, Methods[i].functionPtr);
	}
	
	system("pause");
    return 0;
}


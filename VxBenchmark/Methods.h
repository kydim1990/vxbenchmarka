#define _CRT_SECURE_NO_WARNINGS

#ifndef Methods_H
#define Methods_H
#include <string>
#include <wbemidl.h>
#include <windows.h>

#pragma warning(disable: 4996)

//libcpmt.lib

using namespace std;
//Methods for get info

//isDebug 
extern bool isDebug;

//Class of methods
 class Cl_Methods
{
public:
	char* Name;
	char* PseudoName;
	int(*functionPtr)();
	Cl_Methods(char* _name, char* _pseudoname, int(*_functionPtr)())
	{
		Name = new char[strlen(_name)];
		PseudoName = new char[strlen(_pseudoname)];
		strcpy(Name, _name);
		strcpy(PseudoName, _pseudoname);
		functionPtr = _functionPtr;
	}
};

//get_vendor
string get_vendor();

//get_brand
string get_brand();

//check
void check(char * text, int (*callback)());

//exists_vb_key
int exists_vb_key(LPCWCHAR, LPCWSTR, WCHAR *);

//exists_reg_key
int exists_reg_key(LPCWSTR reg_key);

//HARDWARE_DEVICEMAP_Scsi   --HARDWARE\\DEVICEMAP\\Scsi\\Scsi Port 0\\Scsi Bus 0\\Target Id 0\\Logical Unit
int HARDWARE_DEVICEMAP_Scsi();

//SystemBiosVersion	--SystemBiosVersion
int SystemBiosVersion();

//VirtualBox_Guest_Additions	--VirtualBox Guest Additions
int VirtualBox_Guest_Additions();

//VideoBiosVersion	--VideoBiosVersion
int VideoBiosVersion();

//ACPI_DSDT_VBOX_	--ACPI\\DSDT\\VBOX
int ACPI_DSDT_VBOX();

//ACPI_FADT_VBOX__	--ACPI\\FADT\\VBOX
int ACPI_FADT_VBOX();

//ACPI_RSDT_VBOX	--ACPI\\RSDT\\VBOX
int ACPI_RSDT_VBOX();

//ControlSet001_Services	--ControlSet001\\Services
int ControlSet001_Services();

//SystemBiosDate	--SystemBiosDate
int SystemBiosDate();

//drivers	--drivers
int drivers();

//system32_vbox	--system32\\vbox
int system32_vbox();

//check_mac_x08_x00_x27  --check_mac "\x08\x00\x27"
int check_mac_x08_x00_x27();

//pseudo_devices  --pseudo devices
int pseudo_devices();

//VBoxTray_windows  --VBoxTray windows
int VBoxTray_windows();

//VirtualBox_Shared_Folders  --VirtualBox Shared Folders
int VirtualBox_Shared_Folders();

//vboxservice_vboxtray  --vboxservice/vboxtray
int vboxservice_vboxtray();

//VBox_devices_WMI  --VBox devices using WMI
int VBox_devices_WMI();

//NICCONFIG
int NICCONFIG();

//sysdriver
int sysdriver();

//NTEventLog
int NTEventLog();

//bios
int bios();

//diskdrive
int diskdrive();

//Startup
int Startup();

//ComputerSystem
int ComputerSystem();

//service
int service();

//LogicalDisk
int LogicalDisk();

//Win32_LocalProgramGroup
int Win32_LocalProgramGroup();

//NIC
int NIC();

//process
int process();

//BaseBoard
int BaseBoard();

//SystemEnclosure
int SystemEnclosure();

//cdrom
int cdrom();

//netclient
int netclient();

//csproduct
int csproduct();

//Win32_VideoController
int Win32_VideoController();

//Win32_PnPEntity
int Win32_PnPEntity();
#endif
#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ws2tcpip.h>
#include <windows.h>
#include "Methods.h"
#include <winsock2.h>
#include <ctype.h>
#include <iphlpapi.h>
#include <tlhelp32.h>
#include <wbemidl.h>
#include <stdint.h>
#include <cwchar>


#pragma comment(lib, "ws2_32.lib")
#pragma comment(lib, "iphlpapi.lib")
#pragma comment(lib, "wbemuuid.lib")
#pragma comment(lib, "Mpr.lib")

//isDebug
bool isDebug=true;

//DisableFsRedirectionProto
typedef BOOL(WINAPI *DFsRP) (void*);

//RevertFsRedirectionProto
typedef BOOL(WINAPI *RFsRP) (void*);

//IsProcessProto
typedef BOOL(WINAPI *IsPP) (HANDLE, BOOL*);

//check_row_wmi
typedef int(*check_row_wmi) (IWbemClassObject *);

using namespace std;


//Name of file for logging VB
char * VB_Log = "VB_A";

//class cpu_vender 
class CPUID {
	uint32_t regs[4];

public:
	explicit CPUID(unsigned i) {
		__cpuid((int *)regs, (int)i);
	}

	const uint32_t &EAX() const { return regs[0]; }
	const uint32_t &EBX() const { return regs[1]; }
	const uint32_t &ECX() const { return regs[2]; }
	const uint32_t &EDX() const { return regs[3]; }
};

//get_vendor
string get_vendor()
{
	CPUID cpuID(0);
	string s;
	s += string((const char *)&cpuID.EBX(), 4);
	s += string((const char *)&cpuID.EDX(), 4);
	s += string((const char *)&cpuID.ECX(), 4);
	return s;
}

//get_brand
string  get_brand()
{
	char CPUName[49];

	CPUName[0] = 0;
	__cpuid((int*)&CPUName[0], 0x80000002);

	__cpuid((int*)&CPUName[16], 0x80000003);

	__cpuid((int*)&CPUName[32], 0x80000004);

	CPUName[48] = 0;

	return string(CPUName);
}
//Traccer_log
void Traccer_log(char trace_file[]) {
	FILE *trace;
	trace = fopen(trace_file, "a");
	fclose(trace);
}

//check
void check(char * text, int (*callback)()/*, char * text_log*/)
{
	printf("	%s ___ ", text);
	if (callback() == TRUE) {
		if (isDebug)
			printf("Failed!\n");
		else
			printf("0\n");
		Traccer_log(VB_Log);
	}
	else
	{
		if (isDebug)
			printf("OK\n");
		else
			printf("1\n");
	}
	//
}

//_iswow64
int _iswow64() {
	int res = FALSE;

	IsPP iswow = (IsPP)GetProcAddress(GetModuleHandleA("kernel32"), "IsWow64Process");

	return (iswow) && (iswow(GetCurrentProcess(), &res) != 0) ? res : FALSE;
}

//disable_redirection
int disable_redirection(void * OLD) {
	DFsRP fndisable = (DFsRP)GetProcAddress(
		GetModuleHandleA("kernel32"), "Wow64DisableWow64FsRedirection");

	return (fndisable) && (fndisable(OLD) != 0) ? TRUE : FALSE;
}

//revert_redirection
int revert_redirection(void * OLD) {
	RFsRP fnrevert = (RFsRP)GetProcAddress(
		GetModuleHandleA("kernel32"), "Wow64RevertWow64FsRedirection");

	return (fnrevert) && (fnrevert(OLD) != 0) ? TRUE : FALSE;
}

//check_mac
int check_mac(char * mac) {
	unsigned long list_size = 0, Gaadd;

	Gaadd = GetAdaptersAddresses(AF_UNSPEC, 0, 0, 0, &list_size);

	if (Gaadd == ERROR_BUFFER_OVERFLOW) {
		IP_ADAPTER_ADDRESSES* list = (IP_ADAPTER_ADDRESSES*)LocalAlloc(LMEM_ZEROINIT, list_size);
		if (list) {
			GetAdaptersAddresses(AF_UNSPEC, 0, 0, list, &list_size);
			char cmac[6] = { 0 };
			while (list) {
				if (list->PhysicalAddressLength == 0x6) {
					memcpy(cmac, list->PhysicalAddress, 0x6);
					if (!memcmp(mac, cmac, 3)) { 
						LocalFree(list);
						return TRUE;
					}
				}
				list = list->Next;
			}
			LocalFree(list);
		}
	}
	return FALSE;
}

//check_row
int vb_check_row(IWbemClassObject *row) {
	VARIANT val;
	CIMTYPE cim_type = CIM_ILLEGAL;

	HRESULT hresult = row->Get(L"DeviceId", 0, &val, &cim_type, 0);

	if (FAILED(hresult) || V_VT(&val) == VT_NULL || cim_type != CIM_STRING) {
		return FALSE;
	}

	return (wcsstr(V_BSTR(&val), L"PCI\\VEN_80EE&DEV_CAFE") != NULL) ? TRUE : FALSE;
}

//init_wmi
int init_wmi(const wchar_t *query_namespace, IWbemServices **services) {
	IWbemLocator *Iwlocator = NULL;
	BSTR space;
	int res;

	HRESULT hresult = CoInitializeEx(0, COINIT_MULTITHREADED);

	if (FAILED(hresult)) {
		return FALSE;
	}

	hresult = CoInitializeSecurity(NULL, -1, NULL, NULL, RPC_C_AUTHN_LEVEL_DEFAULT,
		RPC_C_IMP_LEVEL_IMPERSONATE, NULL, EOAC_NONE, NULL);

	if (FAILED(hresult)) {
		CoUninitialize();

		return FALSE;
	}

	hresult = CoCreateInstance(CLSID_WbemLocator, 0, CLSCTX_INPROC_SERVER,
		IID_IWbemLocator, (LPVOID *)& Iwlocator);

	if (FAILED(hresult)) {
		CoUninitialize();

		return FALSE;
	}

	space = SysAllocString(query_namespace);

	hresult = Iwlocator->ConnectServer(space, NULL, NULL, NULL, 0,
		NULL, NULL, services);

	res = FAILED(hresult) ? FALSE : TRUE;

	SysFreeString(space);
	Iwlocator->Release();
	return res;
}

//check_query_wmi
int check_query_wmi(IWbemServices *services, const wchar_t *language, const wchar_t *query, check_row_wmi check_row) {
	int res = FALSE;
	IEnumWbemClassObject *queryrows = NULL;
	BSTR lang = SysAllocString(language);
	BSTR query_wmi = SysAllocString(query);


	HRESULT result = services->ExecQuery(
		lang, query_wmi, WBEM_FLAG_BIDIRECTIONAL, NULL, &queryrows);

	if (!FAILED(result) && (queryrows != NULL)) {
		IWbemClassObject * batchrows[10];
		ULONG index, count = 0;
		result = WBEM_S_NO_ERROR;

		while (WBEM_S_NO_ERROR == result && res == FALSE) {

			result = queryrows->Next(WBEM_INFINITE, 10,
				batchrows, &count);

			if (!SUCCEEDED(result)) {
				continue;
			}

			for (index = 0; index < count && res == FALSE; index++) {
				res = check_row(batchrows[index]);

				batchrows[index]->Release();
			}
		}

		queryrows->Release();
	}

	SysFreeString(query_wmi);
	SysFreeString(lang);

	return res;
}

//exists_vb_key
int exists_vb_key(LPCWCHAR regkey_s, LPCWCHAR value_s, WCHAR * lookup)
{
	HKEY hKey = HKEY_LOCAL_MACHINE;

	HKEY reg_key;
	LONG RQ;
	DWORD size;
	
	WCHAR value[1024];//, *lookup_str;
	
	size_t lookup_size = wcslen(lookup);

	size = sizeof(value);

	if (_iswow64()) {
		RQ = RegOpenKeyEx(hKey, regkey_s, 0, KEY_READ | KEY_WOW64_64KEY, &reg_key);
	}
	else {
		RQ = RegOpenKeyEx(hKey, regkey_s, 0, KEY_READ, &reg_key);
	}
	if (RQ == ERROR_SUCCESS)
	{
		RQ = RegQueryValueEx(reg_key, value_s, NULL, NULL, (LPBYTE)&value, &size);
		RegCloseKey(reg_key);
		if (RQ == ERROR_SUCCESS) {
			size_t i;
			for (i = 0; i < wcslen(value); i++)
				value[i] = toupper(value[i]);

			if (wcsstr(value, lookup) != NULL) {

				return TRUE;
			}
		}
	}
	return FALSE;
}

//exists_reg_key
int exists_reg_key(LPCWSTR regkey_s) {
	HKEY hKey = HKEY_LOCAL_MACHINE;
	
	HKEY reg_key;
	LONG ROKE;

	if (_iswow64()) {
		ROKE = RegOpenKeyEx(hKey, regkey_s, 0, KEY_READ | KEY_WOW64_64KEY, &reg_key);
	}
	else {
		ROKE = RegOpenKeyEx(hKey, regkey_s, 0, KEY_READ, &reg_key);
	}
	if (ROKE == ERROR_SUCCESS) {
		RegCloseKey(reg_key);
		return TRUE;
	}
	else
		return FALSE;
}

//exists_file
int exists_file(LPCWSTR filename) {
	DWORD res = INVALID_FILE_ATTRIBUTES;

	if (_iswow64() == TRUE) {
		void *OLD = NULL;

		if (disable_redirection(&OLD)) {
			res = GetFileAttributes(filename);

			revert_redirection(OLD);
		}
	}
	else {
		res = GetFileAttributes(filename);
	}

	return (res != INVALID_FILE_ATTRIBUTES) ? TRUE : FALSE;
}

//HARDWARE_DEVICEMAP_Scsi   --HARDWARE\\DEVICEMAP\\Scsi\\Scsi Port 0\\Scsi Bus 0\\Target Id 0\\Logical Unit
int HARDWARE_DEVICEMAP_Scsi()
{
	return exists_vb_key(L"HARDWARE\\DEVICEMAP\\Scsi\\Scsi Port 0\\Scsi Bus 0\\Target Id 0\\Logical Unit Id 0", L"Identifier", L"VBOX");
}

//SystemBiosVersion  --SystemBiosVersion
int SystemBiosVersion() {
	return exists_vb_key( L"HARDWARE\\Description\\System", L"SystemBiosVersion", L"VBOX");
}

//VirtualBox_Guest_Additions  --VirtualBox Guest Additions
int VirtualBox_Guest_Additions() {
	return exists_reg_key(L"SOFTWARE\\Oracle\\VirtualBox Guest Additions");
}

//VideoBiosVersion  --VideoBiosVersion
int VideoBiosVersion() {
	return exists_vb_key( L"HARDWARE\\Description\\System", L"VideoBiosVersion", L"VIRTUALBOX");
}

//ACPI_DSDT_VBOX  --ACPI\\DSDT\\VBOX
int ACPI_DSDT_VBOX() {
	return exists_reg_key(L"HARDWARE\\ACPI\\DSDT\\VBOX__");
}

//ACPI_FADT_VBOX  --ACPI\\FADT\\VBOX
int ACPI_FADT_VBOX() {
	return exists_reg_key(L"HARDWARE\\ACPI\\FADT\\VBOX__");
}

//ACPI_RSDT_VBOX  --ACPI\\RSDT\\VBOX
int ACPI_RSDT_VBOX() {
	return exists_reg_key(L"HARDWARE\\ACPI\\RSDT\\VBOX__");
}

//ControlSet001_Services  --ControlSet001\\Services
int ControlSet001_Services() {
	int res = FALSE;
	const int count = 5;

	LPCWSTR strs[count];
	strs[0] = L"SYSTEM\\ControlSet001\\Services\\VBoxGuest";
	strs[1] = L"SYSTEM\\ControlSet001\\Services\\VBoxMouse";
	strs[2] = L"SYSTEM\\ControlSet001\\Services\\VBoxService";
	strs[3] = L"SYSTEM\\ControlSet001\\Services\\VBoxSF";
	strs[4] = L"SYSTEM\\ControlSet001\\Services\\VBoxVideo";

	for (int i = 0; i < count; i++) {
		if (exists_reg_key(strs[i])) {
			res = TRUE;
		}
	}
	return res;
}

//SystemBiosDate  --SystemBiosDate
int SystemBiosDate() {
	return exists_vb_key( L"HARDWARE\\DESCRIPTION\\System", L"SystemBiosDate", L"06/23/99");
}

//drivers	--drivers
int drivers() {
	int res = FALSE;
	const int count = 4;
	LPCWSTR driver_str[count] = {L"C:\\WINDOWS\\system32\\drivers\\VBoxMouse.sys"
								,L"C:\\WINDOWS\\system32\\drivers\\VBoxGuest.sys"
								,L"C:\\WINDOWS\\system32\\drivers\\VBoxSF.sys"
								,L"C:\\WINDOWS\\system32\\drivers\\VBoxVideo.sys"
								};
	
	for (int i = 0; i < count; i++) {
		if (exists_file(driver_str[i])) {
			res = TRUE;
		}
	}
	return res;
}

//system32_vbox	--system32\\vbox..
int system32_vbox() {
	int res = FALSE;
	const int count = 14;
	LPCWSTR strs[count] = {
		L"C:\\WINDOWS\\system32\\vboxdisp.dll"
		,L"C:\\WINDOWS\\system32\\vboxhook.dll"
		,L"C:\\WINDOWS\\system32\\vboxmrxnp.dll"
		,L"C:\\WINDOWS\\system32\\vboxogl.dll"
		,L"C:\\WINDOWS\\system32\\vboxoglarrayspu.dll"
		,L"C:\\WINDOWS\\system32\\vboxoglcrutil.dll"
		,L"C:\\WINDOWS\\system32\\vboxoglerrorspu.dll"
		,L"C:\\WINDOWS\\system32\\vboxoglfeedbackspu.dll"
		,L"C:\\WINDOWS\\system32\\vboxoglpackspu.dll"
		,L"C:\\WINDOWS\\system32\\vboxoglpassthroughspu.dll"
		,L"C:\\WINDOWS\\system32\\vboxservice.exe"
		,L"C:\\WINDOWS\\system32\\vboxtray.exe"
		,L"C:\\WINDOWS\\system32\\VBoxControl.exe"
		,L"C:\\program files\\oracle\\virtualbox guest additions\\"
	};

	for (int i = 0; i < count; i++) {
		if (exists_file(strs[i])) {
			res = TRUE;
		}
	}
	return res;
}

//check_mac_x08_x00_x27	--check_mac "\x08\x00\x27"
int check_mac_x08_x00_x27() {
	return check_mac("\x08\x00\x27");
}

//pseudo_devices	--pseudo devices
int pseudo_devices() {
	int res = FALSE;
	HANDLE handle;
	const int count = 4;

	LPCWSTR strs[count] = {
		L"\\\\.\\VBoxMiniRdrDN"
		,L"\\\\.\\pipe\\VBoxMiniRdDN"
		,L"\\\\.\\VBoxTrayIPC"
		,L"\\\\.\\pipe\\VBoxTrayIPC"
	};
	

	for (int i = 0; i < count; i++) {
		handle = CreateFile(strs[i], GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
		if (handle != INVALID_HANDLE_VALUE) {
			res = TRUE;
		}
	}
	return res;
}

//VBoxTray_windows	--VBoxTray windows
int VBoxTray_windows() {

	if (FindWindow(L"VBoxTrayToolWndClass", NULL) || FindWindow(NULL, L"VBoxTrayToolWnd")) 
		return TRUE;
	else 
		return FALSE;
}

//VirtualBox_Shared_Folders	--VirtualBox Shared Folders
int VirtualBox_Shared_Folders() {
	DWORD size = 0x1000;

	WCHAR  *Provider=new WCHAR[size];

	DWORD WNGPN = WNetGetProviderName(WNNC_NET_RDR2SAMPLE, Provider, &size);

	/*for (int i = 0;i < size;i++)
	{
		printf("%c", Provider[i]);
	}

	printf("\n");*/

	if (WNGPN == NO_ERROR) {
		if (lstrcmpi(Provider, L"VirtualBox Shared Folders") == 0)
		{	
			delete Provider;
			return TRUE;
		}	
		else
		{
			delete Provider;
			return FALSE;
		}	
	}
	delete Provider;
	return FALSE;
}

//vboxservice_vboxtray	--vboxservice/vboxtray
int vboxservice_vboxtray() {
	int res = FALSE;
	HANDLE handle = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	PROCESSENTRY32 Prory32;

	if (handle != INVALID_HANDLE_VALUE) 
		Prory32.dwSize = sizeof(PROCESSENTRY32);
	else 
		return FALSE;

	if (!Process32First(handle, &Prory32)) {
		CloseHandle(handle);
		return FALSE;
	}

	do {
		if (lstrcmpi(Prory32.szExeFile, L"vboxservice.exe") == 0) {
			res = TRUE;
		}
		if (lstrcmpi(Prory32.szExeFile, L"vboxtray.exe") == 0) {
			res = TRUE;
		}
	} while (Process32Next(handle, &Prory32));
	return res;
}

//VBox_devices_WMI	--VBox devices using WMI
int VBox_devices_WMI() {
	IWbemServices *services = NULL;

	if (init_wmi(L"root\\cimv2", &services) != TRUE) {
		return FALSE;
	}

	int result = check_query_wmi(services, L"WQL", L"SELECT DeviceId FROM Win32_PnPEntity",&vb_check_row);

	if (services != NULL) {
		services->Release();
	}
	CoUninitialize();

	return result;
}

//NICCONFIG
int NICCONFIG()
{
	IWbemServices *services = NULL;

	if (init_wmi(L"root\\cimv2", &services) != TRUE) {
		return FALSE;
	}

	int res = FALSE;
	IEnumWbemClassObject *queryrows = NULL;
	BSTR lang = SysAllocString(L"WQL");
	BSTR query_wmi = SysAllocString(L"SELECT * FROM Win32_NetworkAdapterConfiguration");

	HRESULT result = services->ExecQuery(lang, query_wmi, WBEM_FLAG_BIDIRECTIONAL, NULL, &queryrows);

	if (!FAILED(result) && (queryrows != NULL)) {
		IWbemClassObject * batchrows[10];
		ULONG index, count = 0;
		result = WBEM_S_NO_ERROR;

		while (WBEM_S_NO_ERROR == result && res == FALSE) {
			result = queryrows->Next(WBEM_INFINITE, 10,	batchrows, &count);

			if (!SUCCEEDED(result)) {
				continue;
			}

			for (index = 0; index < count && res == FALSE; index++) {;
				VARIANT val,val_desc;
				CIMTYPE cim_type = CIM_ILLEGAL;

				HRESULT hresult = batchrows[index]->Get(L"MACAddress", 0, &val, &cim_type, 0);
				HRESULT hresult_desc = batchrows[index]->Get(L"Description", 0, &val_desc, &cim_type, 0);


				if (SUCCEEDED(hresult) && SUCCEEDED(hresult_desc) && val.bstrVal!=NULL && val_desc.bstrVal != NULL)
				{
					if ( wcsstr(CharLower(val.bstrVal), L"08:00:27:")!= NULL
						&& wcsstr(CharLower(val_desc.bstrVal), L"virtualbox") ==NULL)
						res=TRUE;
				}

				//wprintf(L"TEST  %s\n.", CharLower(val.bstrVal));

				batchrows[index]->Release();
			}
		}

		queryrows->Release();
	}

	SysFreeString(query_wmi);
	SysFreeString(lang);
	CoUninitialize();
	return res;
}

//Win32_SystemDriver aka sysdriver
int sysdriver()
{
	IWbemServices *services = NULL;

	if (init_wmi(L"root\\cimv2", &services) != TRUE) {
		return FALSE;
	}

	int res = FALSE;
	IEnumWbemClassObject *queryrows = NULL;
	BSTR lang = SysAllocString(L"WQL");
	BSTR query_wmi = SysAllocString(L"SELECT * FROM Win32_SystemDriver");

	HRESULT result = services->ExecQuery(lang, query_wmi, WBEM_FLAG_BIDIRECTIONAL, NULL, &queryrows);

	if (!FAILED(result) && (queryrows != NULL)) {
		IWbemClassObject * batchrows[10];
		ULONG index, count = 0;
		result = WBEM_S_NO_ERROR;

		while (WBEM_S_NO_ERROR == result && res == FALSE) {
			result = queryrows->Next(WBEM_INFINITE, 10, batchrows, &count);

			if (!SUCCEEDED(result)) {
				continue;
			}

			for (index = 0; index < count && res == FALSE; index++) {
				
				VARIANT val;
				CIMTYPE cim_type = CIM_ILLEGAL;

				HRESULT hDescription = batchrows[index]->Get(L"Description", 0, &val, &cim_type, 0);

				//Description
				if (SUCCEEDED(hDescription))
				{
					//wprintf(L"TEST  %s\n", CharLower(val.bstrVal));
					if (wcscmp(val.bstrVal, L"VirtualBox Guest Driver") == 0 ||
						wcscmp(val.bstrVal, L"VirtualBox Guest Mouse Service") == 0 ||
						wcscmp(val.bstrVal, L"VirtualBox Shared Folders") == 0 ||
						wcscmp(val.bstrVal, L"VBoxVideo") == 0
						)
						res= TRUE;
				}

				HRESULT hDisplayName = batchrows[index]->Get(L"DisplayName", 0, &val, &cim_type, 0);
				//DisplayName
				if (SUCCEEDED(hDisplayName))
				{
					//wprintf(L"TEST  %s\n", CharLower(val.bstrVal));
					if (wcscmp(val.bstrVal, L"VirtualBox Guest Driver") == 0 ||
						wcscmp(val.bstrVal, L"VirtualBox Guest Mouse Service") == 0 ||
						wcscmp(val.bstrVal, L"VirtualBox Shared Folders") == 0 ||
						wcscmp(val.bstrVal, L"VBoxVideo") == 0
						)
						res= TRUE;
				}

				//Name
				HRESULT hName = batchrows[index]->Get(L"Name", 0, &val, &cim_type, 0);
				if (SUCCEEDED(hName))
				{
					//wprintf(L"TEST  %s\n", CharLower(val.bstrVal));
					if (wcscmp(val.bstrVal, L"VBoxGuest")==0 ||
						wcscmp(val.bstrVal, L"VBoxMouse")== 0 ||
						wcscmp(val.bstrVal, L"VBoxSF") == 0 ||
						wcscmp(val.bstrVal, L"VBoxVideo") == 0
						)
						res= TRUE;
				}

				//PathName
				HRESULT hPathName = batchrows[index]->Get(L"PathName", 0, &val, &cim_type, 0);
				if (SUCCEEDED(hPathName) && val.bstrVal!=NULL)
				{
					//wprintf(L"TEST  %s\n", CharLower(val.bstrVal));
					if (wcsstr(CharLower(val.bstrVal), L"sysevent") != NULL ||
						wcsstr(CharLower(val.bstrVal), L"sysevent") != NULL ||
						wcsstr(CharLower(val.bstrVal), L"vboxsf.sys") != NULL ||
						wcsstr(CharLower(val.bstrVal), L"vboxvideo.sys") != NULL
						)
						res= TRUE;
				}

				batchrows[index]->Release();
			}
		}

		queryrows->Release();
	}

	SysFreeString(query_wmi);
	SysFreeString(lang);
	CoUninitialize();
	return res;
}

//NTEventLog
int NTEventLog()
{
	IWbemServices *services = NULL;

	if (init_wmi(L"root\\cimv2", &services) != TRUE) {
		return FALSE;
	}

	int res = FALSE;
	IEnumWbemClassObject *queryrows = NULL;
	BSTR lang = SysAllocString(L"WQL");
	BSTR query_wmi = SysAllocString(L"SELECT * FROM Win32_NTEventlogFile");

	HRESULT result = services->ExecQuery(lang, query_wmi, WBEM_FLAG_BIDIRECTIONAL, NULL, &queryrows);

	if (!FAILED(result) && (queryrows != NULL)) {
		IWbemClassObject * batchrows[10];
		ULONG index, count = 0;
		result = WBEM_S_NO_ERROR;

		while (WBEM_S_NO_ERROR == result && res == FALSE) {
			result = queryrows->Next(WBEM_INFINITE, 10, batchrows, &count);

			if (!SUCCEEDED(result)) {
				continue;
			}

			for (index = 0; index < count && res == FALSE; index++) {

				VARIANT val;
				CIMTYPE cim_type = CIM_ILLEGAL;

				CIMTYPE cim_type_1 = CIM_FLAG_ARRAY;

				HRESULT hFileName = batchrows[index]->Get(L"FileName", 0, &val, &cim_type, 0);

				//FileName
				if (SUCCEEDED(hFileName))
				{
					if (wcscmp(CharLower(val.bstrVal), L"sysevent") == 0 ||
						wcscmp(CharLower(val.bstrVal), L"system") == 0
						)
					{
						VARIANT val_arr;
						// Sources
						HRESULT hSources = batchrows[index]->Get(L"Sources", 0, &val_arr, &cim_type_1, 0);

						VARIANT var;
						
						long lCount = val_arr.parray->rgsabound[0].cElements;
						long arCount = sizeof(*(val_arr.parray));
						for (long i = 0; i<lCount; i++)
						{
							BSTR str = new OLECHAR[1024];
							SafeArrayGetElement(val_arr.parray, &i, &str);
							
							//wprintf(L"TEST  %s\n", str);
							//wprintf(L"TEST 2 %s\n", CharLowerW(str));

							if (wcscmp(CharLower(str),L"vboxvideo")==0)
								res = TRUE;
						}
						VariantClear(&var);
						
					}
					
				}

				//wprintf(L"TEST  %s\n.", CharLower(val.bstrVal));

				batchrows[index]->Release();
			}
		}

		queryrows->Release();
	}

	SysFreeString(query_wmi);
	SysFreeString(lang);
	CoUninitialize();
	return res;
}

//bios
int bios()
{
	IWbemServices *services = NULL;

	if (init_wmi(L"root\\cimv2", &services) != TRUE) {
		wprintf(L"TEST FALSE\n");
		return FALSE;
	}

	int res = FALSE;
	IEnumWbemClassObject *queryrows = NULL;
	BSTR lang = SysAllocString(L"WQL");
	BSTR query_wmi = SysAllocString(L"SELECT * FROM Win32_BIOS");

	HRESULT result = services->ExecQuery(lang, query_wmi, WBEM_FLAG_BIDIRECTIONAL, NULL, &queryrows);
	
	if (!FAILED(result) && (queryrows != NULL)) {
		IWbemClassObject * batchrows[10];
		ULONG index, count = 0;
		result = WBEM_S_NO_ERROR;

		while (WBEM_S_NO_ERROR == result && res == FALSE) {
			result = queryrows->Next(WBEM_INFINITE, 10, batchrows, &count);

			if (!SUCCEEDED(result)) {
				continue;
			}

			for (index = 0; index < count && res == FALSE; index++) {
				
				VARIANT val;
				CIMTYPE cim_type = CIM_ILLEGAL;
				
				HRESULT hManufacturer = batchrows[index]->Get(L"Manufacturer", 0, &val, &cim_type, 0);

				//Manufacturer
				if (SUCCEEDED(hManufacturer))
				{
					//wprintf(L"TEST  %s\n", CharLower(val.bstrVal));
					if (wcsstr(CharLower(val.bstrVal), L"innotek gmbh") != NULL)
						res= TRUE;
				}

				//SMBIOSBIOSVersion
				HRESULT hSMBIOSBIOSVersion = batchrows[index]->Get(L"SMBIOSBIOSVersion", 0, &val, &cim_type, 0);

				if (SUCCEEDED(hSMBIOSBIOSVersion))
				{
					//wprintf(L"TEST  %s\n", CharLower(val.bstrVal));
					if (wcsstr(CharLower(val.bstrVal), L"virtualbox") != NULL)
						res= TRUE;
				}

				//Version
				HRESULT hVersion = batchrows[index]->Get(L"Version", 0, &val, &cim_type, 0);

				if (SUCCEEDED(hVersion))
				{
					//wprintf(L"TEST  %s\n", CharLower(val.bstrVal));
					if (wcsstr(CharLower(val.bstrVal), L"vbox   - 1") != NULL)
						res= TRUE;
				}
				//wprintf(L"TEST  %s\n.", CharLower(val.bstrVal));

				batchrows[index]->Release();
			}
		}

		queryrows->Release();
	}

	SysFreeString(query_wmi);
	SysFreeString(lang);
	CoUninitialize();
	return res;
}

//diskdrive
int diskdrive()
{
	IWbemServices *services = NULL;

	if (init_wmi(L"root\\cimv2", &services) != TRUE) {
		wprintf(L"TEST FALSE\n");
		return FALSE;
	}

	int res = FALSE;
	IEnumWbemClassObject *queryrows = NULL;
	BSTR lang = SysAllocString(L"WQL");
	BSTR query_wmi = SysAllocString(L"SELECT * FROM Win32_DiskDrive");

	HRESULT result = services->ExecQuery(lang, query_wmi, WBEM_FLAG_BIDIRECTIONAL, NULL, &queryrows);

	if (!FAILED(result) && (queryrows != NULL)) {
		IWbemClassObject * batchrows[10];
		ULONG index, count = 0;
		result = WBEM_S_NO_ERROR;

		while (WBEM_S_NO_ERROR == result && res == FALSE) {
			result = queryrows->Next(WBEM_INFINITE, 10, batchrows, &count);

			if (!SUCCEEDED(result)) {
				continue;
			}

			for (index = 0; index < count && res == FALSE; index++) {

				VARIANT val;
				CIMTYPE cim_type = CIM_ILLEGAL;

				HRESULT hModel = batchrows[index]->Get(L"Model", 0, &val, &cim_type, 0);

				//Model
				if (SUCCEEDED(hModel))
				{
					//wprintf(L"TEST  %s\n", CharLower(val.bstrVal));
					if (wcscmp(CharLower(val.bstrVal), L"vbox harddisk") == 0)
						res = TRUE;
				}

				//PNPDeviceID
				HRESULT hPNPDeviceID = batchrows[index]->Get(L"PNPDeviceID", 0, &val, &cim_type, 0);
				if (SUCCEEDED(hPNPDeviceID))
				{
					//wprintf(L"TEST  %s\n", CharLower(val.bstrVal));
					if (wcsstr(CharLower(val.bstrVal), L"diskvbox") != NULL)
						res = TRUE;
				}



				batchrows[index]->Release();
			}
		}

		queryrows->Release();
	}

	SysFreeString(query_wmi);
	SysFreeString(lang);
	CoUninitialize();
	return res;
}

//Startup
int Startup()
{
	IWbemServices *services = NULL;

	if (init_wmi(L"root\\cimv2", &services) != TRUE) {
		wprintf(L"TEST FALSE\n");
		return FALSE;
	}

	int res = FALSE;
	IEnumWbemClassObject *queryrows = NULL;
	BSTR lang = SysAllocString(L"WQL");
	BSTR query_wmi = SysAllocString(L"SELECT * FROM Win32_StartupCommand");

	HRESULT result = services->ExecQuery(lang, query_wmi, WBEM_FLAG_BIDIRECTIONAL, NULL, &queryrows);

	if (!FAILED(result) && (queryrows != NULL)) {
		IWbemClassObject * batchrows[10];
		ULONG index, count = 0;
		result = WBEM_S_NO_ERROR;

		while (WBEM_S_NO_ERROR == result && res == FALSE) {
			result = queryrows->Next(WBEM_INFINITE, 10, batchrows, &count);

			if (!SUCCEEDED(result)) {
				continue;
			}

			for (index = 0; index < count && res == FALSE; index++) {

				VARIANT val;
				CIMTYPE cim_type = CIM_ILLEGAL;

				HRESULT hCaption = batchrows[index]->Get(L"Caption", 0, &val, &cim_type, 0);

				//Caption
				if (SUCCEEDED(hCaption))
				{
					//wprintf(L"TEST  %s\n", CharLower(val.bstrVal));
					if (wcscmp(CharLower(val.bstrVal), L"vboxtray") == 0)
						res = TRUE;
				}

				//Command
				HRESULT hCommand = batchrows[index]->Get(L"Command", 0, &val, &cim_type, 0);
				if (SUCCEEDED(hCommand))
				{
					//wprintf(L"TEST  %s\n", CharLower(val.bstrVal));
					if (wcsstr(CharLower(val.bstrVal), L"vboxtray.exe") != NULL)
						res = TRUE;
				}
				
				//Description
				HRESULT hDescription = batchrows[index]->Get(L"Description", 0, &val, &cim_type, 0);
				if (SUCCEEDED(hDescription))
				{
					//wprintf(L"TEST  %s\n", CharLower(val.bstrVal));
					if (wcscmp(CharLower(val.bstrVal), L"vboxtray") == 0)
						res = TRUE;
				}

				batchrows[index]->Release();
			}
		}

		queryrows->Release();
	}

	SysFreeString(query_wmi);
	SysFreeString(lang);
	CoUninitialize();
	return res;
}

//ComputerSystem
int ComputerSystem()
{
	IWbemServices *services = NULL;

	if (init_wmi(L"root\\cimv2", &services) != TRUE) {
		wprintf(L"TEST FALSE\n");
		return FALSE;
	}

	int res = FALSE;
	IEnumWbemClassObject *queryrows = NULL;
	BSTR lang = SysAllocString(L"WQL");
	BSTR query_wmi = SysAllocString(L"SELECT * FROM Win32_ComputerSystem");

	HRESULT result = services->ExecQuery(lang, query_wmi, WBEM_FLAG_BIDIRECTIONAL, NULL, &queryrows);

	if (!FAILED(result) && (queryrows != NULL)) {
		IWbemClassObject * batchrows[10];
		ULONG index, count = 0;
		result = WBEM_S_NO_ERROR;

		while (WBEM_S_NO_ERROR == result && res == FALSE) {
			result = queryrows->Next(WBEM_INFINITE, 10, batchrows, &count);

			if (!SUCCEEDED(result)) {
				continue;
			}

			for (index = 0; index < count && res == FALSE; index++) {

				VARIANT val;
				CIMTYPE cim_type = CIM_ILLEGAL;

				HRESULT hManufacturer = batchrows[index]->Get(L"Manufacturer", 0, &val, &cim_type, 0);

				//Manufacturer
				if (SUCCEEDED(hManufacturer))
				{
					//wprintf(L"TEST  %s\n", CharLower(val.bstrVal));
					if (wcscmp(CharLower(val.bstrVal), L"innotek gmbh") == 0)
						res = TRUE;
				}

				//Model
				HRESULT hModel = batchrows[index]->Get(L"Model", 0, &val, &cim_type, 0);
				if (SUCCEEDED(hModel))
				{
					//wprintf(L"TEST  %s\n", CharLower(val.bstrVal));
					if (wcscmp(CharLower(val.bstrVal), L"virtualbox") == 0)
						res = TRUE;
				}

				//OEMStringArray
				HRESULT hOEMStringArray = batchrows[index]->Get(L"OEMStringArray", 0, &val, &cim_type, 0);
				if (SUCCEEDED(hOEMStringArray))
				{
				    //wprintf(L"TEST  %s\n", CharLower(val.bstrVal));
					if (wcsstr(CharLower(val.bstrVal), L"vboxver_") != NULL ||
						wcsstr(CharLower(val.bstrVal), L"vboxrev_") != NULL
						)
					{
						VARIANT val_arr;
						// OEM
						HRESULT hOEM = batchrows[index]->Get(L"OEM", 0, &val_arr, &cim_type, 0);

					    VARIANT var;

						long lCount = val_arr.parray->rgsabound[0].cElements;
						long arCount = sizeof(*(val_arr.parray));
						for (long i = 0; i < lCount; i++)
						{
							BSTR str = new OLECHAR[1024];
							SafeArrayGetElement(val_arr.parray, &i, &str);

							//wprintf(L"TEST  %s\n", str);
							//wprintf(L"TEST 2 %s\n", CharLowerW(str));

							if (wcsstr(CharLower(str), L"vboxver_") !=NULL ||
								wcsstr(CharLower(str), L"vboxrev_") != NULL
								)
								res = TRUE;
						}
						VariantClear(&var);
						res = TRUE;
					}
				}

				batchrows[index]->Release();
			}
		}

		queryrows->Release();
	}

	SysFreeString(query_wmi);
	SysFreeString(lang);
	CoUninitialize();
	return res;
}

//service
int service()
{
	IWbemServices *services = NULL;

	if (init_wmi(L"root\\cimv2", &services) != TRUE) {
		wprintf(L"TEST FALSE\n");
		return FALSE;
	}

	int res = FALSE;
	IEnumWbemClassObject *queryrows = NULL;
	BSTR lang = SysAllocString(L"WQL");
	BSTR query_wmi = SysAllocString(L"SELECT * FROM Win32_Service");

	HRESULT result = services->ExecQuery(lang, query_wmi, WBEM_FLAG_BIDIRECTIONAL, NULL, &queryrows);

	if (!FAILED(result) && (queryrows != NULL)) {
		IWbemClassObject * batchrows[10];
		ULONG index, count = 0;
		result = WBEM_S_NO_ERROR;

		while (WBEM_S_NO_ERROR == result && res == FALSE) {
			result = queryrows->Next(WBEM_INFINITE, 10, batchrows, &count);

			if (!SUCCEEDED(result)) {
				continue;
			}

			for (index = 0; index < count && res == FALSE; index++) {

				VARIANT val;
				CIMTYPE cim_type = CIM_ILLEGAL;

				HRESULT hCaption = batchrows[index]->Get(L"Caption", 0, &val, &cim_type, 0);

				//Caption
				if (SUCCEEDED(hCaption))
				{
					//wprintf(L"TEST  %s\n", CharLower(val.bstrVal));
					if (wcscmp(CharLower(val.bstrVal), L"virtualbox guest additions service") == 0)
						res = TRUE;
				}

				//DisplayName
				HRESULT hDisplayName = batchrows[index]->Get(L"DisplayName", 0, &val, &cim_type, 0);
				if (SUCCEEDED(hDisplayName))
				{
					//wprintf(L"TEST  %s\n", CharLower(val.bstrVal));
					if (wcscmp(CharLower(val.bstrVal), L"virtualbox guest additions service") == 0)
						res = TRUE;
				}

				//Name
				HRESULT hName = batchrows[index]->Get(L"Name", 0, &val, &cim_type, 0);
				if (SUCCEEDED(hName))
				{
					//wprintf(L"TEST  %s\n", CharLower(val.bstrVal));
					if (wcscmp(CharLower(val.bstrVal), L"vboxservice") == 0	)
						res = TRUE;
				}

				//PathName
				HRESULT hPathName = batchrows[index]->Get(L"PathName", 0, &val, &cim_type, 0);
				if (SUCCEEDED(hPathName) && val.bstrVal!=NULL)
				{
					//wprintf(L"TEST  %s\n", CharLower(val.bstrVal));
					if (wcsstr(CharLower(val.bstrVal), L"vboxservice.exe") != NULL)
						res = TRUE;
				}

				batchrows[index]->Release();
			}
		}

		queryrows->Release();
	}

	SysFreeString(query_wmi);
	SysFreeString(lang);
	CoUninitialize();
	return res;
}

//LogicalDisk
int LogicalDisk()
{
	IWbemServices *services = NULL;

	if (init_wmi(L"root\\cimv2", &services) != TRUE) {
		wprintf(L"TEST FALSE\n");
		return FALSE;
	}

	int res = FALSE;
	IEnumWbemClassObject *queryrows = NULL;
	BSTR lang = SysAllocString(L"WQL");
	BSTR query_wmi = SysAllocString(L"SELECT * FROM Win32_LogicalDisk");

	HRESULT result = services->ExecQuery(lang, query_wmi, WBEM_FLAG_BIDIRECTIONAL, NULL, &queryrows);

	if (!FAILED(result) && (queryrows != NULL)) {
		IWbemClassObject * batchrows[10];
		ULONG index, count = 0;
		result = WBEM_S_NO_ERROR;

		while (WBEM_S_NO_ERROR == result && res == FALSE) {
			result = queryrows->Next(WBEM_INFINITE, 10, batchrows, &count);

			if (!SUCCEEDED(result)) {
				continue;
			}

			for (index = 0; index < count && res == FALSE; index++) {

				VARIANT val, valdr;
				CIMTYPE cim_type = CIM_ILLEGAL;

				HRESULT hDriveType = batchrows[index]->Get(L"DriveType", 0, &valdr, &cim_type, 0);

				//DriveType
				if (SUCCEEDED(hDriveType))
				{
					//wprintf(L"TEST  %s\n", CharLower(val.bstrVal));
					if (valdr.intVal == 3)
					{
						//VolumeSerialNumber
						HRESULT hVolumeSerialNumber = batchrows[index]->Get(L"VolumeSerialNumber", 0, &val, &cim_type, 0);
						if (SUCCEEDED(hVolumeSerialNumber))
						{
							//wprintf(L"TEST  %s\n", CharLower(val.bstrVal));
							if (wcscmp(CharLower(val.bstrVal), L"fceae0a3") == 0)
								res = TRUE;
						}
					}

					if (valdr.intVal == 5)
					{
						//VolumeName
						HRESULT hVolumeName = batchrows[index]->Get(L"VolumeName", 0, &val, &cim_type, 0);
						if (SUCCEEDED(hVolumeName) && val.bstrVal != NULL)
						{
							//wprintf(L"TEST  %s\n", CharLower(val.bstrVal));
							if (wcsstr(CharLower(val.bstrVal), L"vboxadditions") != NULL)
								res = TRUE;
						}
					}
				}

				batchrows[index]->Release();
			}
		}

		queryrows->Release();
	}

	SysFreeString(query_wmi);
	SysFreeString(lang);
	CoUninitialize();
	return res;
}

//Win32_LocalProgramGroup
int Win32_LocalProgramGroup()
{
	IWbemServices *services = NULL;

	if (init_wmi(L"root\\cimv2", &services) != TRUE) {
		wprintf(L"TEST FALSE\n");
		return FALSE;
	}

	int res = FALSE;
	IEnumWbemClassObject *queryrows = NULL;
	BSTR lang = SysAllocString(L"WQL");
	BSTR query_wmi = SysAllocString(L"SELECT * FROM Win32_LogicalProgramGroup");

	HRESULT result = services->ExecQuery(lang, query_wmi, WBEM_FLAG_BIDIRECTIONAL, NULL, &queryrows);

	if (!FAILED(result) && (queryrows != NULL)) {
		IWbemClassObject * batchrows[10];
		ULONG index, count = 0;
		result = WBEM_S_NO_ERROR;

		while (WBEM_S_NO_ERROR == result && res == FALSE) {
			result = queryrows->Next(WBEM_INFINITE, 10, batchrows, &count);

			if (!SUCCEEDED(result)) {
				continue;
			}

			for (index = 0; index < count && res == FALSE; index++) {

				VARIANT val;
				CIMTYPE cim_type = CIM_ILLEGAL;

				HRESULT hName = batchrows[index]->Get(L"Name", 0, &val, &cim_type, 0);

				//Name
				if (SUCCEEDED(hName))
				{
					//wprintf(L"TEST  %s\n", CharLower(val.bstrVal));
					if (wcsstr(CharLower(val.bstrVal), L"oracle vm virtualbox guest additions") != NULL)
						res = TRUE;
				}

				batchrows[index]->Release();
			}
		}

		queryrows->Release();
	}

	SysFreeString(query_wmi);
	SysFreeString(lang);
	CoUninitialize();
	return res;
}

//NIC
int NIC()
{
	IWbemServices *services = NULL;

	if (init_wmi(L"root\\cimv2", &services) != TRUE) {
		wprintf(L"TEST FALSE\n");
		return FALSE;
	}

	int res = FALSE;
	IEnumWbemClassObject *queryrows = NULL;
	BSTR lang = SysAllocString(L"WQL");
	BSTR query_wmi = SysAllocString(L"SELECT * FROM Win32_NetworkAdapter");

	HRESULT result = services->ExecQuery(lang, query_wmi, WBEM_FLAG_BIDIRECTIONAL, NULL, &queryrows);

	if (!FAILED(result) && (queryrows != NULL)) {
		IWbemClassObject * batchrows[10];
		ULONG index, count = 0;
		result = WBEM_S_NO_ERROR;

		while (WBEM_S_NO_ERROR == result && res == FALSE) {
			result = queryrows->Next(WBEM_INFINITE, 10, batchrows, &count);

			if (!SUCCEEDED(result)) {
				continue;
			}

			for (index = 0; index < count && res == FALSE; index++) {

				VARIANT val, val_Description;
				CIMTYPE cim_type = CIM_ILLEGAL;

				HRESULT hMACAddress = batchrows[index]->Get(L"MACAddress", 0, &val, &cim_type, 0);

				HRESULT hDescription = batchrows[index]->Get(L"Description", 0, &val_Description, &cim_type, 0);

				//MACAddress 
				if (SUCCEEDED(hMACAddress) && SUCCEEDED(hDescription) && val.bstrVal!=NULL && val_Description.bstrVal != NULL)
				{
					//wprintf(L"TEST  %s\n", CharLower(val.bstrVal));
					//wprintf(L"TEST  %s\n", CharLower(val_Description.bstrVal));
					if (wcsstr(CharLower(val.bstrVal), L"08:00:27:") != NULL &&
						wcsstr(CharLower(val_Description.bstrVal), L"virtualbox") != NULL
						
						)
						res = TRUE;
					
				}

				batchrows[index]->Release();
			}
		}

		queryrows->Release();
	}

	SysFreeString(query_wmi);
	SysFreeString(lang);
	CoUninitialize();
	return res;
}

//process
int process()
{
	IWbemServices *services = NULL;

	if (init_wmi(L"root\\cimv2", &services) != TRUE) {
		wprintf(L"TEST FALSE\n");
		return FALSE;
	}

	int res = FALSE;
	IEnumWbemClassObject *queryrows = NULL;
	BSTR lang = SysAllocString(L"WQL");
	BSTR query_wmi = SysAllocString(L"SELECT * FROM Win32_Process");

	HRESULT result = services->ExecQuery(lang, query_wmi, WBEM_FLAG_BIDIRECTIONAL, NULL, &queryrows);

	if (!FAILED(result) && (queryrows != NULL)) {
		IWbemClassObject * batchrows[10];
		ULONG index, count = 0;
		result = WBEM_S_NO_ERROR;

		while (WBEM_S_NO_ERROR == result && res == FALSE) {
			result = queryrows->Next(WBEM_INFINITE, 10, batchrows, &count);

			if (!SUCCEEDED(result)) {
				continue;
			}

			for (index = 0; index < count && res == FALSE; index++) {

				VARIANT val;
				CIMTYPE cim_type = CIM_ILLEGAL;

				HRESULT hDescription = batchrows[index]->Get(L"Description", 0, &val, &cim_type, 0);

				//Description 
				if (SUCCEEDED(hDescription) )
				{
					//wprintf(L"TEST  %s\n", CharLower(val.bstrVal));
					if (wcscmp(CharLower(val.bstrVal), L"vboxservice.exe") == 0 ||
						wcscmp(CharLower(val.bstrVal), L"vboxtray.exe") == 0
						)
						res = TRUE;
					
				}

				//Name
				HRESULT hName = batchrows[index]->Get(L"Name", 0, &val, &cim_type, 0);
				if (SUCCEEDED(hName))
				{
					//wprintf(L"TEST  %s\n", CharLower(val.bstrVal));
					if (wcscmp(CharLower(val.bstrVal), L"vboxservice.exe") == 0 ||
						wcscmp(CharLower(val.bstrVal), L"vboxtray.exe") == 0
						)
						res = TRUE;
					
				}

				//CommandLine
				HRESULT hCommandLine = batchrows[index]->Get(L"CommandLine", 0, &val, &cim_type, 0);
				if (SUCCEEDED(hCommandLine) && val.bstrVal!=NULL)
				{
					//wprintf(L"TEST  %s\n", CharLower(val.bstrVal));
					if (wcsstr(CharLower(val.bstrVal), L"vboxservice.exe") != NULL ||
						wcsstr(CharLower(val.bstrVal), L"vboxtray.exe") != NULL
						)
						res = TRUE;
				}

				//ExecutablePath
				HRESULT hExecutablePath = batchrows[index]->Get(L"ExecutablePath", 0, &val, &cim_type, 0);
				if (SUCCEEDED(hExecutablePath) && val.bstrVal != NULL)
				{
					//wprintf(L"TEST  %s\n", CharLower(val.bstrVal));
					if (wcsstr(CharLower(val.bstrVal), L"vboxservice.exe") != NULL ||
						wcsstr(CharLower(val.bstrVal), L"vboxtray.exe") != NULL
						)
						res = TRUE;
				}

				batchrows[index]->Release();
			}
		}

		queryrows->Release();
	}

	SysFreeString(query_wmi);
	SysFreeString(lang);
	CoUninitialize();
	return res;
}

//BaseBoard
int BaseBoard()
{
	IWbemServices *services = NULL;

	if (init_wmi(L"root\\cimv2", &services) != TRUE) {
		wprintf(L"TEST FALSE\n");
		return FALSE;
	}

	int res = FALSE;
	IEnumWbemClassObject *queryrows = NULL;
	BSTR lang = SysAllocString(L"WQL");
	BSTR query_wmi = SysAllocString(L"SELECT * FROM Win32_BaseBoard");

	HRESULT result = services->ExecQuery(lang, query_wmi, WBEM_FLAG_BIDIRECTIONAL, NULL, &queryrows);

	if (!FAILED(result) && (queryrows != NULL)) {
		IWbemClassObject * batchrows[10];
		ULONG index, count = 0;
		result = WBEM_S_NO_ERROR;

		while (WBEM_S_NO_ERROR == result && res == FALSE) {
			result = queryrows->Next(WBEM_INFINITE, 10, batchrows, &count);

			if (!SUCCEEDED(result)) {
				continue;
			}

			for (index = 0; index < count && res == FALSE; index++) {

				VARIANT val;
				CIMTYPE cim_type = CIM_ILLEGAL;

				HRESULT hManufacturer = batchrows[index]->Get(L"Manufacturer", 0, &val, &cim_type, 0);

				//Manufacturer 
				if (SUCCEEDED(hManufacturer))
				{
					//wprintf(L"TEST  %s\n", CharLower(val.bstrVal));
					if (wcscmp(CharLower(val.bstrVal), L"oracle corporation") == 0)
						res = TRUE;

				}

				//Product
				HRESULT hProduct = batchrows[index]->Get(L"Product", 0, &val, &cim_type, 0);
				if (SUCCEEDED(hProduct))
				{
					//wprintf(L"TEST  %s\n", CharLower(val.bstrVal));
					if (wcscmp(CharLower(val.bstrVal), L"virtualbox") == 0)
						res = TRUE;

				}

				batchrows[index]->Release();
			}
		}

		queryrows->Release();
	}

	SysFreeString(query_wmi);
	SysFreeString(lang);
	CoUninitialize();
	return res;
}


//SystemEnclosure
int SystemEnclosure()
{
	IWbemServices *services = NULL;

	if (init_wmi(L"root\\cimv2", &services) != TRUE) {
		wprintf(L"TEST FALSE\n");
		return FALSE;
	}

	int res = FALSE;
	IEnumWbemClassObject *queryrows = NULL;
	BSTR lang = SysAllocString(L"WQL");
	BSTR query_wmi = SysAllocString(L"SELECT * FROM Win32_SystemEnclosure");

	HRESULT result = services->ExecQuery(lang, query_wmi, WBEM_FLAG_BIDIRECTIONAL, NULL, &queryrows);

	if (!FAILED(result) && (queryrows != NULL)) {
		IWbemClassObject * batchrows[10];
		ULONG index, count = 0;
		result = WBEM_S_NO_ERROR;

		while (WBEM_S_NO_ERROR == result && res == FALSE) {
			result = queryrows->Next(WBEM_INFINITE, 10, batchrows, &count);

			if (!SUCCEEDED(result)) {
				continue;
			}

			for (index = 0; index < count && res == FALSE; index++) {

				VARIANT val;
				CIMTYPE cim_type = CIM_ILLEGAL;

				HRESULT hManufacturer = batchrows[index]->Get(L"Manufacturer", 0, &val, &cim_type, 0);

				//Manufacturer 
				if (SUCCEEDED(hManufacturer))
				{
					//wprintf(L"TEST  %s\n", CharLower(val.bstrVal));
					if (wcscmp(CharLower(val.bstrVal), L"oracle corporation") == 0)
						res = TRUE;

				}


				batchrows[index]->Release();
			}
		}

		queryrows->Release();
	}

	SysFreeString(query_wmi);
	SysFreeString(lang);
	CoUninitialize();
	return res;
}

//cdrom
int cdrom()
{
	IWbemServices *services = NULL;

	if (init_wmi(L"root\\cimv2", &services) != TRUE) {
		wprintf(L"TEST FALSE\n");
		return FALSE;
	}

	int res = FALSE;
	IEnumWbemClassObject *queryrows = NULL;
	BSTR lang = SysAllocString(L"WQL");
	BSTR query_wmi = SysAllocString(L"SELECT * FROM Win32_CDROMDrive");

	HRESULT result = services->ExecQuery(lang, query_wmi, WBEM_FLAG_BIDIRECTIONAL, NULL, &queryrows);

	if (!FAILED(result) && (queryrows != NULL)) {
		IWbemClassObject * batchrows[10];
		ULONG index, count = 0;
		result = WBEM_S_NO_ERROR;

		while (WBEM_S_NO_ERROR == result && res == FALSE) {
			result = queryrows->Next(WBEM_INFINITE, 10, batchrows, &count);

			if (!SUCCEEDED(result)) {
				continue;
			}

			for (index = 0; index < count && res == FALSE; index++) {

				VARIANT val;
				CIMTYPE cim_type = CIM_ILLEGAL;

				HRESULT hName = batchrows[index]->Get(L"Name", 0, &val, &cim_type, 0);

				//Name 
				if (SUCCEEDED(hName))
				{
					//wprintf(L"TEST  %s\n", CharLower(val.bstrVal));
					if (wcscmp(CharLower(val.bstrVal), L"vbox cd-rom") == 0)
						res = TRUE;

				}

				//VolumeName
				HRESULT hVolumeName = batchrows[index]->Get(L"VolumeName", 0, &val, &cim_type, 0);
				if (SUCCEEDED(hVolumeName) && val.bstrVal!=NULL)
				{
					//wprintf(L"TEST  %s\n", CharLower(val.bstrVal));
					if (wcsstr(CharLower(val.bstrVal), L"vboxadditions") != NULL)
						res = TRUE;

				}

				//DeviceID
				HRESULT hDeviceID = batchrows[index]->Get(L"DeviceID", 0, &val, &cim_type, 0);
				if (SUCCEEDED(hDeviceID))
				{
					//wprintf(L"TEST  %s\n", CharLower(val.bstrVal));
					if (wcsstr(CharLower(val.bstrVal), L"cdromvbox") != NULL)
						res = TRUE;

				}

				//PNPDeviceID
				HRESULT hPNPDeviceID = batchrows[index]->Get(L"PNPDeviceID", 0, &val, &cim_type, 0);
				if (SUCCEEDED(hPNPDeviceID))
				{
					//wprintf(L"TEST  %s\n", CharLower(val.bstrVal));
					if (wcsstr(CharLower(val.bstrVal), L"cdromvbox") != NULL)
						res = TRUE;

				}


				batchrows[index]->Release();
			}
		}

		queryrows->Release();
	}

	SysFreeString(query_wmi);
	SysFreeString(lang);
	CoUninitialize();
	return res;
}

//netclient
int netclient()
{
	IWbemServices *services = NULL;

	if (init_wmi(L"root\\cimv2", &services) != TRUE) {
		wprintf(L"TEST FALSE\n");
		return FALSE;
	}

	int res = FALSE;
	IEnumWbemClassObject *queryrows = NULL;
	BSTR lang = SysAllocString(L"WQL");
	BSTR query_wmi = SysAllocString(L"SELECT * FROM WIN32_NetworkClient");

	HRESULT result = services->ExecQuery(lang, query_wmi, WBEM_FLAG_BIDIRECTIONAL, NULL, &queryrows);

	if (!FAILED(result) && (queryrows != NULL)) {
		IWbemClassObject * batchrows[10];
		ULONG index, count = 0;
		result = WBEM_S_NO_ERROR;

		while (WBEM_S_NO_ERROR == result && res == FALSE) {
			result = queryrows->Next(WBEM_INFINITE, 10, batchrows, &count);

			if (!SUCCEEDED(result)) {
				continue;
			}

			for (index = 0; index < count && res == FALSE; index++) {

				VARIANT val;
				CIMTYPE cim_type = CIM_ILLEGAL;

				HRESULT hDescription = batchrows[index]->Get(L"Description", 0, &val, &cim_type, 0);

				//Description 
				if (SUCCEEDED(hDescription))
				{
					//wprintf(L"TEST  %s\n", CharLower(val.bstrVal));
					if (wcscmp(CharLower(val.bstrVal), L"vboxsf") == 0)
						res = TRUE;

				}


				// Manufacturer
				HRESULT hManufacturer = batchrows[index]->Get(L"Manufacturer", 0, &val, &cim_type, 0);

				if (SUCCEEDED(hManufacturer))
				{
					//wprintf(L"TEST  %s\n", CharLower(val.bstrVal));
					if (wcscmp(CharLower(val.bstrVal), L"oracle corporation") == 0)
						res = TRUE;

				}

				//Name
				HRESULT hName = batchrows[index]->Get(L"Name", 0, &val, &cim_type, 0);

				if (SUCCEEDED(hName))
				{
					//wprintf(L"TEST  %s\n", CharLower(val.bstrVal));
					if (wcscmp(CharLower(val.bstrVal), L"virtualbox shared folders") == 0)
						res = TRUE;

				}

				batchrows[index]->Release();
			}
		}

		queryrows->Release();
	}

	SysFreeString(query_wmi);
	SysFreeString(lang);
	CoUninitialize();
	return res;
}

//csproduct
int csproduct()
{
	IWbemServices *services = NULL;

	if (init_wmi(L"root\\cimv2", &services) != TRUE) {
		wprintf(L"TEST FALSE\n");
		return FALSE;
	}

	int res = FALSE;
	IEnumWbemClassObject *queryrows = NULL;
	BSTR lang = SysAllocString(L"WQL");
	BSTR query_wmi = SysAllocString(L"SELECT * FROM Win32_ComputerSystemProduct");

	HRESULT result = services->ExecQuery(lang, query_wmi, WBEM_FLAG_BIDIRECTIONAL, NULL, &queryrows);

	if (!FAILED(result) && (queryrows != NULL)) {
		IWbemClassObject * batchrows[10];
		ULONG index, count = 0;
		result = WBEM_S_NO_ERROR;

		while (WBEM_S_NO_ERROR == result && res == FALSE) {
			result = queryrows->Next(WBEM_INFINITE, 10, batchrows, &count);

			if (!SUCCEEDED(result)) {
				continue;
			}

			for (index = 0; index < count && res == FALSE; index++) {

				VARIANT val;
				CIMTYPE cim_type = CIM_ILLEGAL;

				HRESULT hName = batchrows[index]->Get(L"Name", 0, &val, &cim_type, 0);

				//Name 
				if (SUCCEEDED(hName))
				{
					//wprintf(L"TEST  %s\n", CharLower(val.bstrVal));
					if (wcscmp(CharLower(val.bstrVal), L"virtualbox") == 0)
						res = TRUE;

				}


				// Vendor
				HRESULT hVendor = batchrows[index]->Get(L"Vendor", 0, &val, &cim_type, 0);

				if (SUCCEEDED(hVendor))
				{
					//wprintf(L"TEST  %s\n", CharLower(val.bstrVal));
					if (wcscmp(CharLower(val.bstrVal), L"innotek gmbh") == 0)
						res = TRUE;

				}


				batchrows[index]->Release();
			}
		}

		queryrows->Release();
	}

	SysFreeString(query_wmi);
	SysFreeString(lang);
	CoUninitialize();
	return res;
}

//Win32_VideoController
int Win32_VideoController()
{
	IWbemServices *services = NULL;

	if (init_wmi(L"root\\cimv2", &services) != TRUE) {
		wprintf(L"TEST FALSE\n");
		return FALSE;
	}

	int res = FALSE;
	IEnumWbemClassObject *queryrows = NULL;
	BSTR lang = SysAllocString(L"WQL");
	BSTR query_wmi = SysAllocString(L"SELECT * FROM Win32_VideoController");

	HRESULT result = services->ExecQuery(lang, query_wmi, WBEM_FLAG_BIDIRECTIONAL, NULL, &queryrows);

	if (!FAILED(result) && (queryrows != NULL)) {
		IWbemClassObject * batchrows[10];
		ULONG index, count = 0;
		result = WBEM_S_NO_ERROR;

		while (WBEM_S_NO_ERROR == result && res == FALSE) {
			result = queryrows->Next(WBEM_INFINITE, 10, batchrows, &count);

			if (!SUCCEEDED(result)) {
				continue;
			}

			for (index = 0; index < count && res == FALSE; index++) {

				VARIANT val;
				CIMTYPE cim_type = CIM_ILLEGAL;

				//Name 
				HRESULT hName = batchrows[index]->Get(L"Name", 0, &val, &cim_type, 0);

				if (SUCCEEDED(hName))
				{
					//wprintf(L"TEST:%s:\n", CharLower(val.bstrVal));
					if (wcscmp(CharLower(val.bstrVal), L"virtualbox graphics adapter")==0)
						res = TRUE;

				}

				//VideoController 
				HRESULT hVideoController = batchrows[index]->Get(L"VideoController", 0, &val, &cim_type, 0);

				if (SUCCEEDED(hVideoController))
				{
					//wprintf(L"TEST  %s\n", CharLower(val.bstrVal));
					if (wcscmp(CharLower(val.bstrVal),L"virtualbox graphics adapter")==0)
						res = TRUE;

				}

				// Description
				HRESULT hDescription = batchrows[index]->Get(L"Description", 0, &val, &cim_type, 0);

				if (SUCCEEDED(hDescription))
				{
					//wprintf(L"TEST  %s\n", CharLower(val.bstrVal));
					if (wcscmp(CharLower(val.bstrVal), L"virtualbox graphics adapter")==0)
						res = TRUE;

				}

				// Caption
				HRESULT hCaption = batchrows[index]->Get(L"Caption", 0, &val, &cim_type, 0);

				if (SUCCEEDED(hCaption))
				{
					//wprintf(L"TEST  %s\n", CharLower(val.bstrVal));
					if (wcscmp(CharLower(val.bstrVal), L"virtualbox graphics adapter")==0)
						res = TRUE;
				}

				// VideoProcessor
				HRESULT hVideoProcessor = batchrows[index]->Get(L"VideoProcessor", 0, &val, &cim_type, 0);

				if (SUCCEEDED(hVideoProcessor))
				{
					//wprintf(L"TEST  %s\n", CharLower(val.bstrVal));
					if (wcscmp(CharLower(val.bstrVal), L"vbox")==0)
						res = TRUE;
				}

				// InstalledDisplayDrivers
				HRESULT hInstalledDisplayDrivers = batchrows[index]->Get(L"InstalledDisplayDrivers", 0, &val, &cim_type, 0);

				if (SUCCEEDED(hInstalledDisplayDrivers))
				{
					//wprintf(L"TEST  %s\n", CharLower(val.bstrVal));
					if (wcscmp(CharLower(val.bstrVal), L"vboxdisp.sys")==0)
						res = TRUE;
				}

				// InfSection
				HRESULT hInfSection = batchrows[index]->Get(L"InfSection", 0, &val, &cim_type, 0);

				if (SUCCEEDED(hInfSection))
				{
					//wprintf(L"TEST  %s\n", CharLower(val.bstrVal));
					if (wcscmp(CharLower(val.bstrVal), L"vboxvideo")==0)
						res = TRUE;
				}

				//AdapterCompatibility
				HRESULT hAdapterCompatibility = batchrows[index]->Get(L"AdapterCompatibility", 0, &val, &cim_type, 0);

				if (SUCCEEDED(hAdapterCompatibility))
				{
					//wprintf(L"TEST  %s\n", CharLower(val.bstrVal));
					if (wcscmp(CharLower(val.bstrVal), L"oracle corporation")==0)
						res = TRUE;
				}


				batchrows[index]->Release();
			}
		}

		queryrows->Release();
	}

	SysFreeString(query_wmi);
	SysFreeString(lang);
	CoUninitialize();
	return res;
}

//Win32_PnPEntity
int Win32_PnPEntity()
{
	IWbemServices *services = NULL;

	if (init_wmi(L"root\\cimv2", &services) != TRUE) {
		wprintf(L"TEST FALSE\n");
		return FALSE;
	}

	int res = FALSE;
	IEnumWbemClassObject *queryrows = NULL;
	BSTR lang = SysAllocString(L"WQL");
	BSTR query_wmi = SysAllocString(L"SELECT * FROM Win32_PnPEntity");

	HRESULT result = services->ExecQuery(lang, query_wmi, WBEM_FLAG_BIDIRECTIONAL, NULL, &queryrows);

	if (!FAILED(result) && (queryrows != NULL)) {
		IWbemClassObject * batchrows[10];
		ULONG index, count = 0;
		result = WBEM_S_NO_ERROR;

		while (WBEM_S_NO_ERROR == result && res == FALSE) {
			result = queryrows->Next(WBEM_INFINITE, 10, batchrows, &count);

			if (!SUCCEEDED(result)) {
				continue;
			}

			for (index = 0; index < count && res == FALSE; index++) {

				VARIANT val;
				CIMTYPE cim_type = CIM_ILLEGAL;

				//PnPEntity 
				HRESULT hPnPEntity = batchrows[index]->Get(L"PnPEntity", 0, &val, &cim_type, 0);

				if (SUCCEEDED(hPnPEntity))
				{
					//wprintf(L"TEST  %s\n", CharLower(val.bstrVal));
					if (wcscmp(CharLower(val.bstrVal), L"virtualbox device") ==0 ||
						wcscmp(CharLower(val.bstrVal), L"vbox harddisk") == 0 ||
						wcscmp(CharLower(val.bstrVal), L"vbox cd-rom") == 0 ||
						wcscmp(CharLower(val.bstrVal), L"virtualbox graphics adapter") == 0
						)
						res = TRUE;

				}

				// Caption
				HRESULT hCaption = batchrows[index]->Get(L"Caption", 0, &val, &cim_type, 0);

				if (SUCCEEDED(hCaption) && val.bstrVal!=NULL)
				{
					//wprintf(L"TEST  %s\n", CharLower(val.bstrVal));
					if (wcscmp(CharLower(val.bstrVal), L"virtualbox device") ==0||
						wcscmp(CharLower(val.bstrVal), L"vbox harddisk") == 0 ||
						wcscmp(CharLower(val.bstrVal), L"vbox cd-rom") == 0 ||
						wcscmp(CharLower(val.bstrVal), L"virtualbox graphics adapter") == 0
						)
						res = TRUE;

				}

				// Description
				HRESULT hDescription = batchrows[index]->Get(L"Description", 0, &val, &cim_type, 0);

				if (SUCCEEDED(hDescription) && val.bstrVal!=NULL)
				{
					//wprintf(L"TEST  %s\n", CharLower(val.bstrVal));
					if (wcscmp(CharLower(val.bstrVal), L"virtualbox device")==0 ||
						wcscmp(CharLower(val.bstrVal),L"virtualbox graphics adapter") ==0
						)
						res = TRUE;
				}

				// Manufacturer
				/*HRESULT hManufacturer = batchrows[index]->Get(L"Manufacturer", 0, &val, &cim_type, 0);

				if (SUCCEEDED(hManufacturer))
				{
					//wprintf(L"TEST  %s\n", CharLower(val.bstrVal));
					if (CharLower(val.bstrVal)== L"oracle corporation")
						res = TRUE;
				}*/

				// Service
				HRESULT hService = batchrows[index]->Get(L"Service", 0, &val, &cim_type, 0);

				if (SUCCEEDED(hService) && val.bstrVal!=NULL)
				{
					//wprintf(L"TEST  %s\n", CharLower(val.bstrVal));
					if (wcscmp(CharLower(val.bstrVal), L"vboxguests") ==0 ||
						wcscmp(CharLower(val.bstrVal), L"vboxvideo") ==0
						)
						res = TRUE;
				}

				// DeviceID
				HRESULT hDeviceID = batchrows[index]->Get(L"DeviceID", 0, &val, &cim_type, 0);

				if (SUCCEEDED(hDeviceID))
				{
					//wprintf(L"TEST  %s\n", CharLower(val.bstrVal));
					if (wcsstr(CharLower(val.bstrVal), L"diskvbox_") != NULL ||
						wcsstr(CharLower(val.bstrVal), L"cdromvbox_") != NULL
						)
						res = TRUE;
				}

				//PNPDeviceID
				HRESULT hPNPDeviceID = batchrows[index]->Get(L"PNPDeviceID", 0, &val, &cim_type, 0);

				if (SUCCEEDED(hPNPDeviceID))
				{
					//wprintf(L"TEST  %s\n", CharLower(val.bstrVal));
					if (wcsstr(CharLower(val.bstrVal), L"diskvbox_") !=NULL ||
						wcsstr(CharLower(val.bstrVal), L"cdromvbox_") !=NULL
						)
						res = TRUE;
				}


				batchrows[index]->Release();
			}
		}

		queryrows->Release();
	}

	SysFreeString(query_wmi);
	SysFreeString(lang);
	CoUninitialize();
	return res;
}
 